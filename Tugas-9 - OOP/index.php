<?php

require('animal.php');
require('Ape.php');
require('Frog.php');

$sheep = new Animal("shaun");
$sungokong = new Ape("kera sakti");
$kodok = new Frog("buduk");

echo "Name : " . $sheep->name . "<br>"; // "shaun"
echo "Legs : " . $sheep->legs . "<br>"; // 4
echo "Cold Blooded : " . $sheep->cold_blooded . "<br><br>"; // "no"

echo "Name : " . $kodok->name . "<br>"; 
echo "Legs : " . $kodok->legs . "<br>"; 
echo "Cold Blooded : " . $kodok->cold_blooded . "<br>"; 
echo "Jump : ";
echo $kodok->jump() . "<br><br>";

echo "Name : " . $sungokong->name . "<br>";
echo "Legs : " . $sungokong->legs . "<br>"; 
echo "Cold Blooded : " . $sungokong->cold_blooded . "<br>"; 
echo "Yell : ";
echo $sungokong->yell() . "<br><br>";

